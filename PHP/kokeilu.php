<?php

require("./dbconn.php");


// valmistetaan kysely
$kysely = $yhteys->prepare("SELECT * FROM new_table");
// suoritetaan kysely
$kysely->execute();

$result = $kysely->fetchAll(PDO::FETCH_ASSOC);
// käsitellään tulostaulun rivit yksi kerrallaan

print_r($result);
while ($result) {
    $rivi = $result[0];

    echo $rivi["etunimi"]." ".$rivi["sukunimi"]."\n";
    array_shift($result);
}

?>
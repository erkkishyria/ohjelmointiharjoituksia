<?php

require_once("./dbconn.php");
const LOKIFILE = "loki.txt";

$ktunnus = "person1";
$ssana = md5("1234");

kirjoitaLokia(LOKIFILE, "Saatu käyttäjätunnus ja salasana");

$sql = "SELECT * FROM new_table WHERE ktunnus = :ktunnus AND ssana = :ssana";
$stmt = $yhteys->prepare($sql); 
$stmt->bindValue(":ktunnus", $ktunnus, PDO::PARAM_STR); 
$stmt->bindValue(":ssana", $ssana, PDO::PARAM_STR);
$stmt->execute(); 
kirjoitaLokia(LOKIFILE, "Kysely onnistui, salasana: '$ssana', käyttäjätunnus: $ktunnus");

$result = $stmt->fetchAll(PDO::FETCH_ASSOC); 

if($result) {
    echo "Found user with password";
    kirjoitaLokia(LOKIFILE, "Sisäänkirjautuminen onnistui");
} else {
    echo "Did not find user with password";
    kirjoitaLokia($lokifile, "Sisäänkirjautuminen ei onnistunut");
}

function kirjoitaLokia($fileName, $teksti) {
    $fileStream = fopen($fileName, "a+"); 
    fwrite($fileStream, $teksti."\r\n");
    fclose($fileStream);
}
?> 
const urObject  = {
    attr1: "Ensimmäinen attribuutti",
    attr2: "Toinen attribuutti"
};

function objNappi() {
    urObject.omaMetodi = function () {
        console.log(this.attr1); 
    }
    urObject.omaMetodi();
}

function primitiveButton() {
    urObject = {}; 
}

/**
 * Privimitive data types
 * String 

As the name suggests, the string is for a sequence of characters, for example, “demo”, “Hi”, etc. 

Boolean 

It has two values i.e True or False.

Number 

Number represents numerical values such as 5, 20, 500, etc.

Undefined

Undefined value

Null 

Null value

Symbol 

New datatype introduced in ECMAScript 6.
 */
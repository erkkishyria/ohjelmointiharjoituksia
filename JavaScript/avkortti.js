function kortin_arvot(nimi, radio) {
    let otsikko = document.getElementById("nimi");
    otsikko.innerHTML = nimi;
    let radiospan = document.getElementById("radio");
    radiospan.innerHTML = radio;
}


function parametrit() {
    const queryString = window.location.search;
    console.log(queryString);
    const urlParams = new URLSearchParams(queryString);
    const nimi = urlParams.get('nimi');
    console.log(nimi);
    const radio = urlParams.get('joku_radio');
    kortin_arvot(nimi, radio);
}

window.onload = parametrit; 
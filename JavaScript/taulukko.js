function toiminne() {

    let taulukko = [1, 2, 3, 4, 5];

    const uusitaulukko = taulukko.map(value => {
        return value*2;
    });

    console.log(uusitaulukko);

    const toinentaulukko = taulukko.map(function (element) {
        return element*3; 
    });

    console.log(toinentaulukko); 
}

const arr = ["banaani", "omena", "mandariini", "appelsiini", "kurkku", "tomaatti", "peruna"];

/**
 * Tulostaa jokaisen elementin, jos se sisältää "i"
 */
function tulostusI() {
    // const tulos = arr.filter(value => value.indexOf('i') > -1);
    // console.log(tulos); 

    const tulos = [];
    for (let index = 0; index < arr.length; index++) {
        const element = arr[index];
        if(element.indexOf("i") > -1) {
            tulos.push(element);
        }
    }
    console.log(tulos);
}

function jarjesta() {
    console.log(arr.sort());
}

function poistaEka() {
    console.log(arr.shift());
    console.log(arr);
}

function puske() {
    arr.push("sipuli");
    console.log(arr);
}









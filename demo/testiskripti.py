# Kirjoita ohjelma, joka pyytää käyttäjältä 2 lukua ja laskee niiden summan, 
# erotuksen, tulon ja osamäärän.

luku1 = int(input("Anna kokonaisluku1: "))
luku2 = int(input("Anna kokonaisluku2: "))
print(f"Summa ({luku1}+{luku2}) on: {luku1 + luku2}")
print(f"Erotus ({luku1} - {luku2}) on: {luku1 - luku2}")
print(f"Tulo ({luku1}*{luku2}) on: {luku1 * luku2}")
print(f"Osamäärä ({luku1}/{luku2}) on: {luku1 / luku2}")


# "Jatka edellistä tehtävää siten, että ohjelma pyytää käyttäjältä vielä viisi lukua 
# ja ohjelma tulostaa, mikä annetuista luvuista oli suurin ja pienin. "
luvut = [luku1, luku2]
for x in range(5): 
    luvut.append(int(input(f"Anna vielä {x+3}:s luku: ")))


pienin = min(luvut)
suurin = max(luvut)

print(f"Suurin oli: {suurin}")
print(f"Pienin oli: {pienin}")

luku = int(input("Anna luonnollinen luku: "))

# alkuluku on jaollinen vain ykkösellä ja itsellään

if luku in range(1,4): 
    onAlkuluku = True
elif luku % 2 == 0:
    onAlkuluku = False
else: #brute force
    onAlkuluku = True
    for i in range(3, luku, 2):
        if luku % i == 0: 
            onAlkuluku = False
            break
if onAlkuluku: 
    print(f"{luku} on alkuluku")
else: 
    print(f"{luku } ei ole alkuluku")
#for-looppi pysähtyy käyttäjän antamaan numeroon
import sys

print("Ohjelma tulostaa luvut väliltä 1-10, mutta pysähtyy antamaasi numeroon.")
luku = int(input("Anna luonnollinen luku väliltä 1 - 10:"))

if(luku < 1 or luku > 10): #virheellisen syötteen tarkistus
    print("Ei tämän näin pitänyt mennä...")
    sys.exit()

for i in range(1, 11):
    print(i) 
    if i == luku: 
        break

import datetime as dt

kello = dt.datetime.now().hour

if (kello <7): 
    print("Vielä voi jatkaa unia.")
elif (kello <= 8):
    print("Aika nousta luennolle.")
elif (kello <= 12): 
    print("Nukuin pahasti pommiin, mutta vielä kerkeää iltapäivän opiskella")
else: 
    print("Tuli nukuttua pommista yli")
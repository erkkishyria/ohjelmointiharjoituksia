# jatka pakollista tehtävää 6 siten, että ohjelma
# tarkistaa syntymävuoden perusteella, onko käyttäjä 
# täysi-ikäinen tänään

import datetime as dt

tänään = dt.date.today()

nimi = input("Syötä nimesi: ")
spaiva = input("Anna syntymäpäiväsi määrämuotoisena (esim. '2008-08-22'): ")
# kotikaupunki = input("Mikä on kotikaupunkisi: ")
# kotimaa = input("Mikä on kotimaasi: ")

slista = spaiva.split("-")
svuosi = int(slista[0])
skk = int(slista[1])
spv = int(slista[2])

#Perjantaina klo 11 riisipuuro ja sitten kotiin :)

täysi_ikäinen_tänään = False  
if (tänään.year - svuosi) == 18:
    if (tänään.month - skk) == 0:
        if (tänään.day - spv) >= 0: 
            täysi_ikäinen_tänään = True
    elif (tänään.month - skk) > 0: 
        täysi_ikäinen_tänään = True
elif (tänään.year - svuosi) > 18: 
    täysi_ikäinen_tänään = True

if täysi_ikäinen_tänään: 
    print(f"Hei {nimi}! Olet täysi-ikäinen tänään!")
else: 
    print("Not today..")

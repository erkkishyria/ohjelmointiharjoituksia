#Kirjoita ohjelma, joka tulostaa silmukassa luvut kahdesta 
# kolmeenkymmeneen kahden luvun välein. Jokainen luku tulostetaan omalle rivilleen.
# Ohjelman tulosteen alku näytää siis tältä:
#
# Esimerkkitulostus
# 2
# 4
# 6
# 8
# jne...

luku = 2
while luku <= 30: 
    print(luku)
    luku += 2
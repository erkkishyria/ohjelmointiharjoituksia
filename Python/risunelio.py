import viiva

def risunelio(n): 
    kertaa = n
    while n > 0: 
        viiva.viiva(kertaa, "#")
        n -= 1

risunelio(5)
print()
risunelio(3)
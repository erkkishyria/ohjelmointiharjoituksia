class Henkilö: 
    numero = 0

    def __init__(self): 
        Henkilö.numero += 1
        print(f"Henkilö {self.numero}: ")
        self.nimi = input("Nimi: ")
        self.ikä = int(int(input("Ikä: ")))
        return None
    
henkilö1 = Henkilö()
henkilö2 = Henkilö()

if henkilö1.ikä == henkilö2.ikä: 
    print(f"{henkilö1.nimi} ja {henkilö2.nimi} ovat yhtä vanhoja")
elif henkilö1.ikä > henkilö2.ikä: 
    print(f"Vanhempi on {henkilö1.nimi}")
else: 
    print(f"Vanhempi on {henkilö2.nimi}")
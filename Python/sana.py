def eka_sana(lause): 
    i = lause.find(" ")
    if i < 0 and len(lause) > 0: 
        return lause
    elif len(lause) == 0: 
        return ""
    else: 
        return lause[0:i]

def toka_sana(lause): 
    i = lause.find(" ")
    if i < 0 and len(lause) > 0 or len(lause) == 0: 
        return ""
    else: 
        return eka_sana(lause[i+1:])

def vika_sana(lause): 
    i = lause.rfind(" ")
    if i < 0 and len(lause) > 0: 
        return lause
    elif len(lause) == 0: 
        return ""
    else: 
        sana = eka_sana(lause[::-1])
        sana = sana[::-1]
        return sana
    

lause = "olipa kerran elämä"
eka = eka_sana(lause)
toka = toka_sana(lause) 
vika = vika_sana(lause)
print(eka)
print(toka)
print(vika)

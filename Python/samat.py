def samat(merkkijono, i, j): 
    if i >= len(merkkijono) or j >= len(merkkijono): 
        return False
    if merkkijono[i] == merkkijono[j]: 
        return True
    else: 
        return False

# samat merkit o ja o
print(samat("koodari", 1, 2)) # True

# eri merkit k ja a
print(samat("koodari", 0, 4)) # False

# toinen indeksi ei ole merkkijonon sisällä
print(samat("koodari", 0, 10)) # False
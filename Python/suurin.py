def luvuista_suurin(a, b, c):
    lista = [a, b, c]
    lista.sort()
    return lista[2]

print(luvuista_suurin(3, 4, 1)) # 4
print(luvuista_suurin(99, -4, 7)) # 99
print(luvuista_suurin(0, 0, 0)) # 0
import viiva

def nelio(n, merkki): 
    kertaa = n
    while n > 0: 
        viiva.viiva(kertaa, merkki)
        n -= 1

nelio(5, "*")
print()
nelio(3, "o")
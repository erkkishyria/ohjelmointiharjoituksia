import viiva 

def kolmio(n): 
    kertaa = 1
    while kertaa <= n: 
        viiva.viiva(kertaa, "#")
        kertaa += 1

kolmio(6)
print()
kolmio(3)
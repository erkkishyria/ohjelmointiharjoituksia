import csv
import mysql.connector
# Määrittele tietokantaan yhteysparametrit
db_config = {
    'host': 'localhost',
    'user': 'kayttajanimi',
    'password': 'salasana',
    'database': 'classicmodels'
}
try:
    # Luo yhteys tietokantaan
    connection = mysql.connector.connect(**db_config)
    if connection.is_connected():
        print("Yhteys tietokantaan on avattu")
        # Luo kursori tietokantaoperaatioita varten
        cursor = connection.cursor()
        # Luo taulu, joka vastaa CSV-tiedoston rakennetta
        create_table_query = """
        CREATE TABLE IF NOT EXISTS csv_data (
            column1 INT,
            column2 VARCHAR(255),
            column3 DECIMAL(10, 2)
        )
        """
        cursor.execute(create_table_query)
        # Lue CSV-tiedosto
        with open('tiedosto.csv', 'r') as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=';')
            next(csv_reader)  # Ohita otsikkorivi, jos sellainen on
            # Käsittele jokainen rivi ja lisää se tietokantaan
            for row in csv_reader:
                insert_query = "INSERT INTO csv_data (column1, column2, column3) VALUES (%s, %s, %s)"
                cursor.execute(insert_query, (int(row[0]), row[1], float(row[2])))
        # Vahvista muutokset tietokantaan
        connection.commit()
        print("Tiedot on lisätty tietokantaan")
except mysql.connector.Error as e:
    print("Virhe:", e)
finally:
    # Sulje kursori ja yhteys
    if 'cursor' in locals():
        cursor.close()
    if 'connection' in locals() and connection.is_connected():
        connection.close()
        print("Yhteys tietokantaan on suljettu")